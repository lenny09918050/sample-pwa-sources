#!/bin/bash

pushd testing/document-root
ln -sf ../../sources/ressources/img img
ln -sf ../../sources/ressources/manifests/* .
ln -sf ../../sources/ressources/favicons/* .
popd

echo 0
